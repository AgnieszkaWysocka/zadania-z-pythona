from flask import Flask
app = Flask(__name__)

counter = 0

@app.route('/number')
def number():
    return str(counter)

@app.route('/up')
def increment():
    global counter
    counter += 1
    return str(counter)

@app.route('/down')
def decrement():
    global counter
    counter -= 1
    return str(counter)

app.run(port=5000, debug=True)
