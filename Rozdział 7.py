print("Zadanie 7.1")

mystery = '\U0001f4a9'

print(mystery)

import unicodedata

print(unicodedata.name(mystery))

print("----------")
print("Zadanie 7.2")

pop_bytes = mystery.encode('utf-8')

print(pop_bytes)

print("----------")
print("Zadanie 7.3")

pop_string = pop_bytes.decode('utf-8')

print(pop_string)

print(pop_string == mystery)

print("----------")
print("Zadanie 7.4")

'My kitty cat likes %s,' % ('roast')
'My kitty cat likes %s,' % ('ham')
'My kitty cat fell on his %s' % ('head')
"And now thinks he\'s a %s." % ('clam')

print('My kitty cat likes %s,' % ('roast'))
print('My kitty cat likes %s,' % ('ham'))
print('My kitty cat fell on his %s' % ('head'))
print("And now thinks he\'s a %s." % ('clam'))
print("And now thinks he's a %s." % ('clam2'))
print('And now thinks he\'s a %s.' % ('clam3'))

print("----------")
print("Zadanie 7.5")

letter = 'Dear {salutation} {name}, '\
'\v\nThank you for your letter. We are sorry that our {product} {verbed} in your {room}. Please note that it should never be used in a {room}, especially near any {animals}.'\
'\n\nThank you for your letter. We are sorry that our {product} {verbed} in your {room}...'\
'\v\vThank you for your letter. We are sorry that our {product} {verbed} in your {room}...'\
'\v\nSend us your receipt and {amount} for shipping and handling. We will send you another {product} that, in our tests,is {percent}% less likely to have {verbed}. '\
'\v\nThank you for your support. '\
'\v\nSincerely, '\
'\n{spokesman} '\
'\n{job_title} ' 

print(letter)

print("----------")
print("Zadanie 7.6")

response = {
    'salutation' : 'Mister',
    'name' : 'Piotr Zajaczkowski',
    'product' : 'heating chair',
    'verbed' : 'exploded',
    'room' : 'living room',
    'animals' : 'lemurs',
    'amount' : '$10',
    'percent' : 57,
    'spokesman' : 'Agnieszka Wysocka',
    'job_title' : 'CEO of the Heating Department'
}

print(letter.format(**response))

print("----------")
print("Zadanie 7.7")

mammoth = 'We have seen the queen of cheese, '\
'\nLaying quietly at your ease, '\
'\nGently fanned by evening breeze '\
'\nThy fair form no flies dare seize. '\
'\v\nAll gaily dressed soon you\'ll go '\
'\nTo the great Provincial Show, '\
'\nTo be admired by many a beau '\
'\nIn the city of Toronto. '\
'\v\nCows numerous as a swarm of bees '\
'\nOr as the leaves upon the trees '\
'\nIt did require to make thee please, '\
'\nAnd stand unrivalled queen of cheese. '\
'\v\nMay you not receive a scar as '\
'\nWe have heard that Mr. Harris '\
'\nIntends to send you off as far as '\
'\nThe great world\'s show at Paris. '\
'\v\nOf the youth beware of these '\
'\nFor some of them might rudely squeeze '\
'\nAnd bite your cheek; then songs or glees '\
'\nWe could not sing oh queen of cheese. '\
'\v\nWe\'rt thou suspended from baloon, '\
'\nYou\'d cast a shade, even at noon; '\
'\nFolks would think it was the moon '\
'\nAbout to fall and crush them soon.'

print(mammoth)

print("----------")
print("Zadanie 7.8")

import re
search = re.findall(r'\b[c|C][a-z]*\b', mammoth)
print(search)

print("----------")
print("Zadanie 7.9")

import re
search2 = re.findall(r'\b([c|C][a-z]{3})\b', mammoth)
print(search2)

print("----------")
print("Zadanie 7.10")

import re
search3 = re.findall(r'\b([a-zA-z]*r)\b', mammoth)
print(search3)

print("----------")
print("Zadanie 7.11")

import re
search4 = re.findall(r'\b[a-zA-z]*[eyuioa]{3}[a-zA-z]*\b', mammoth)
print(search4)

print("----------")
print("Zadanie 7.12")

hex_string = '47494638396101000100800000000000ffffff21f9'\
'0401000000002c000000000100010000020144003b'

import binascii

gif = binascii.unhexlify(hex_string)
print(gif)

print("----------")
print("Zadanie 7.13")

import struct
valid_GIF_header = b'GIF89a'
if gif[:6] == valid_GIF_header:
    print('Valid gif')
else:
    print('Not a valid GIF')


print("----------")
print("Zadanie 7.14")

import struct
width, height = struct.unpack('<2H', gif[6:10])
print('Width:', width, 'height:', height)

#krotka = (1, 2, 3) - dlatego, jesli nazwiemy to tylko 'width' to wydrukuje sie wartosc (256,) bo to bedzie krotka
#k1, k2, k3 = (1, 2, 3)
#print(krotka)
#print(k1, k2, k3)