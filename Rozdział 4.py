print("Zadanie 4.1")

guess_me = 7
if guess_me < 7:
    print("too low")
elif guess_me > 7:
    print("too high")
else:
    print("just right")

print("----------")
print("Zadanie 4.2")

guess_me = 7
start = 1
while start <= guess_me:
    if start < guess_me:
        print("too low")
    elif start == guess_me:
        print("found it!")
    else:
        print("oops")
    start += 1

print("----------")
print("Zadanie 4.3")

for number in [3, 2, 1, 0]:
    print(number)

print("----------")
print("Zadanie 4.4")

even_numbers = [number for number in range(10) if number%2==0]
print("even numbers: %s" % even_numbers)

print("----------")
print("Zadanie 4.5")

square = {number : number**2 for number in range(10)}
print(square)

print("----------")
print("Zadanie 4.6")

odd = {number for number in range(10) if number%2==1}
print("odd numbers: %s" % odd)

print("----------")
print("Zadanie 4.7") #nad tym zadaniem troche kombinowalam i masz tutaj moje pomysly

generator = ('Got %s' % number for number in range(10)) #to nie ma sensu
print(generator) #tak tego chyba nie mozna drukowac

for number in range(10):
    print("Got %s" % number) #problem polega na tym, ze w zadaniu prosili o return a nie o print
    number+=1
    
print("Inny sposob rozwiazania")

def gen():
    for number in range(10):
        return("Got ")
        yield number
        print("Got %s" % number) #ale tu sie nic nie drukuje podczas wywolywania funkcji
        number += 1

gen()

def gen(): #to dziala wiec czemu w poprzednim sposobie rozwiazania nie drukuje? ki czort?
    for number in range(10):
        print("Got %s" % number)
        number += 1

gen()

print("----------")
print("Zadanie 4.8")

def good():
    return ['Harry', 'Ron', 'Hermione']
    
print(good())

print("----------")
print("Zadanie 4.9")
     
odds=[number for number in range(10) if number %2 == 1] #jest to niezgodne z trescia zadania ale czyz nie jest o wiele prostsze?
print(odds[2]) #2 bo Python liczy od 0

def get_odds(): #drugie latwiejsze rozwiazanie
    return [number for number in range(10) if number %2 == 1]
print(get_odds()[2]) #jak w prosty sposob wydrukowac trzecia wartosc


def get_odds(): #dzieki pomocy Szamana zadanie rozwiazane zgodnie z trescia
    return [number for number in range(10) if number %2 == 1]
x = get_odds()
for i in range(len(x)):
    if i == 2:
        print(x[i])

get_odds()

print("----------")
print("Zadanie 4.10")

def test(func):
    print("start")
    result = func()
    print(result)
    print("end")
    
@test
def useless_function():
    return "This is a completely useless function which was made only to check if that decorator works"

print("----------")
print("Zadanie 4.11")

#nie jestem pewna czy to jest dobrze, w ksiazce jest na ten temat malo

class OopsException(Exception):
    pass

try:
    raise OopsException()
except OopsException as exc:
    print("Caught an oops")
    
print("----------")
print("Zadanie 4.12")
    
titles = ['Creature of Habit', 'Crewel Fate']
plots = ['A nun turns into a monster', 'A haunted yarn shop']

movies = dict(zip(titles, plots))

print(movies)

print("----------")
print("BONUS z wykorzystaniem komunikatow o bledach")

print("W bloku jest 6 mieszkan o numerach od 0 do 5. Sprawdz, kto mieszka pod danym numerem.")
lista_mieszkancow=["dozorca Wiesio", "pani Krysia", "pan Henio", "wujek Stefcio", "babcia Gienia", "ciocia Zosia"]
while True:
    value = input("Kto mieszka pod numerem: [wcisnij 'z' aby zakonczyc]")
    if value == 'z':
        break
    try:
        mieszkanie = int(value)
        print(lista_mieszkancow[mieszkanie])
    except IndexError as err:
        print("Zly numer mieszkania:", mieszkanie)
    except Exception as other:
        print("Cos wpisano nie tak:", other)