from __future__ import print_function

print("Zadanie 10.1")

from datetime import date
now = date.today()
now_string = now.isoformat()
with open('today.txt', 'wt') as fout:
    print(now_string, file = fout)
#fout = open('today.txt', 'wt')
#fout.write(now_string + '\n')
#fout.close()

print("----------")
print("Zadanie 10.2")

fin = open('today.txt', 'rt')
today_string = fin.read()
print(today_string)
fin.close()

print("----------")
print("Zadanie 10.3")

import time
fmt = "%Y-%m-%d\n"
today = time.strptime(today_string, fmt)
print(today)

print("----------")
print("Zadania 10.4-10.6 dotycza tematow, ktorych mialam nie robic")
print("----------")
print("Zadanie 10.7")

from datetime import date
birthday = date(1991, 5, 17)
print(birthday)

print("----------")
print("Zadanie 10.8")

fmt = "My birthday was on %A, %B %d, %Y"
weekday = birthday.strftime(fmt)
print(weekday)

print("----------")
print("Zadanie 10.9")

from datetime import timedelta
one_day = timedelta(days=1)
print(birthday + 10000*one_day)