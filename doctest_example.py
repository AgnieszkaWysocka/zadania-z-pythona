def multiply(a, b):
    """
    >>> multiply(2, 3)
    6
    >>> multiply(3, 3)
    9
    """
    return a * b

if __name__ == '__main__':
	liczba1 = int(input("Podaj pierwsza liczbe: "))
	liczba2 = int(input("Podaj druga liczbe: "))
	print("Wynik mnozenia:")
	print(multiply(liczba1, liczba2))

##############################
# Przyklady uzycia           #
##############################
# Odpalenie samego programu:
# $ python3 doctest_example.py 
# Podaj pierwsza liczbe: 12
# Podaj druga liczbe: 1
# Wynik mnozenia:
# 12
#
# $ python3 doctest_example.py 
# Podaj pierwsza liczbe: 22
# Podaj druga liczbe: 23
# Wynik mnozenia:
# 506
#
# Odpalenie testów:
# $ python3 -m doctest -v doctest_example.py 
# Trying:
#     multiply(2, 3)
# Expecting:
#     6
# ok
# Trying:
#     multiply(3, 3)
# Expecting:
#     9
# ok
# 1 items had no tests:
#     doctest_example
# 1 items passed all tests:
#    2 tests in doctest_example.multiply
# 2 tests in 2 items.
# 2 passed and 0 failed.
# Test passed.
