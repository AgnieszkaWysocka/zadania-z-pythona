from flask import Flask
app = Flask(__name__)

@app.route('/number')
def home():
    return "0"

app.run(port=9999, debug=True)