print("Zadanie 6.1")

class Thing():
    pass

print(Thing)

example = Thing()

print(example)

print("----------")
print("Zadanie 6.2")

class Thing2():
    letters = 'abc'

print(Thing2.letters)

print("----------")
print("Zadanie 6.3")

class Thing3():
    def __init__(self):
        self.letters = 'xyz'

stefan = Thing3()

print(stefan.letters)

print("----------")
print("Zadanie 6.4")

class Element():
    def __init__(self, name, symbol, number):
        self.name = name
        self.symbol = symbol
        self.number = number

Hydrogen = Element('Hydrogen', 'H', '1')

print(Hydrogen.name)
print(Hydrogen.symbol)
print(Hydrogen.number)

print("----------")
print("Zadanie 6.5")

hydrogendict = {'name' : 'Hydrogen', 'symbol' : 'H', 'number' : '1'}

hydrogen2 = Element(**hydrogendict)

print(hydrogen2.name)
print(hydrogen2.symbol)
print(hydrogen2.number)

print("----------")
print("Zadanie 6.6")

class Element():
    def __init__(self, name, symbol, number):
        self.name = name
        self.symbol = symbol
        self.number = number
    def dump(self):
        print(self.name, self.symbol, self.number)

hydrogen = Element('Hydrogen', 'H', '1')

hydrogen.dump()

print("----------")
print("Zadanie 6.7")

print(hydrogen)

class Element():
    def __init__(self, name, symbol, number):
        self.name = name
        self.symbol = symbol
        self.number = number
    def __str__(self):
        return 'Element ' + self.name + ' with symbol ' + self.symbol + ' and number ' + self.number

hydrogen = Element('Hydrogen', 'H', '1')

print(hydrogen)

print("----------")
print("Zadanie 6.8")

class Element():
    def __init__(self, name, symbol, number):
        self.__name = name
        self.__symbol = symbol
        self.__number = number
    @property
    def name(self):
        return self.__name
    @property
    def symbol(self):
        return self.__symbol
    @property
    def number(self):
        return self.__number
    
hydrogen = Element('Hydrogen', 'H', '1')

print(hydrogen.name)
print(hydrogen.symbol)
print(hydrogen.number)

print("----------")
print("Zadanie 6.9")

class Bear():
    @staticmethod
    def eats():
        return 'berries'
        
    
class Rabbit():
    @staticmethod
    def eats():
        return 'clover'

class Octothorpe():
    @staticmethod
    def eats():
        return 'campers'

bear = Bear()

rabbit = Rabbit()

octothorpe = Octothorpe()

print(Bear.eats())
print(Rabbit.eats()) 
print(Octothorpe.eats())

print("----------")
print("Zadanie 6.10")

class Laser():
    def does(self):
        return 'disintegrate'
    
class Claw():
    def does(self):
        return 'crush'

class Smartphone():
    def does(self):
        return 'ring'


class Robot():
    def __init__(self):
        self.laser = Laser()
        self.claw = Claw()
        self.smartphone = Smartphone()


    def does(self):
        print('Robot does: ' + self.laser.does() + ', ' + self.claw.does() + ' and ' + self.smartphone.does())

robot = Robot()
robot.does()