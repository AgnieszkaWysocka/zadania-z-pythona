print("Zadanie 8.1")

test1 = 'This is a test of the emergency text system'

saving = open('test.txt', 'wt')
saving.write(test1)
saving.close()

print("----------")
print("Zadanie 8.2")

reading = open('test.txt', 'rt')
test2 = reading.read()
reading.close()

if test1 == test2:
    print("test1 and test2 are equal")
else:
    print("test1 and test2 are not equal")

print("----------")
print("Zadanie 8.3")

import csv
library = '''author,book
JRR Tolkien,The Hobbit
Lynne Truss,"Eats, Shoots & Leaves"
'''
with open('books.csv', 'wt') as fout:
    fout.write(library)

print("----------")
print("Zadanie 8.4")

import csv
with open('books.csv', 'rt') as fin:
    books = csv.DictReader(fin)
    for book in books:
        print(book)
    
print(books)

print("----------")
print("Zadanie 8.5")

library2 = '''title,author,year
The Weirdstone of Brisingamen,Alan Garner,1960
Perdido Street Station,China Miéville,2000
Thud!,Terry Pratchett,2005
The Spellman Files,Lisa Lutz,2007
Small Gods,Terry Pratchett,1992
'''

import csv
with open('books.csv', 'wt') as fout:
    fout.write(library2)

print("----------")
print("Zadanie 8.6")

import sqlite3
conn = sqlite3.connect('books.db')
conn.execute('''CREATE TABLE book
             (title TEXT PRIMARY KEY,
             author TEXT,
             year INT)''')

print("----------")
print("Zadanie 8.7")

import csv
ins = 'INSERT INTO book (title, author, year) VALUES(?, ?, ?)'
with open('books.csv', 'rt') as fout:
    books = csv.DictReader(fout)
    for book in books:
    	conn.execute(ins, (book['title'], book['author'], book['year']))
conn.commit()
    
print("----------")
print("Zadanie 8.8")

command = conn.execute('SELECT title FROM book ORDER BY title')
for data in command:
    print(data)

print("----------")
print("Zadanie 8.9")

command2 = conn.execute('SELECT * FROM book ORDER BY year')
for data in command2:
    print(data)

print("----------")
print("Zadanie 8.10")

#nie udało mi się zainstalować SQLAlchemy